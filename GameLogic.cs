﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CatsAndMice {
    public class GameLogic : IGameLogic {
        public const int maxFila = 20;
        public const int maxColumna = 40;

        private static readonly int someFruits = 10;
        private static readonly int somePoisons = 10;

        // Los elementos del juego se gestionan en varios objetos contenedores de datos 
        // que apuntan hacia datos comunes.
        // Ventaja: se facilita la impelemntación de la lógica del juego.
        // Inconveniente: Hay que mantener la coherencia entre los diferents contenedores, duplicando
        // las operaciones de inserción y borrado.

        // Panel del juego: contiene referencia a todos los datos.
        // Cada casilla o bien está vacía (contiene referencia a null) o bien
        // contiene una referencia al elemento del juego que está sobre ella.
        private IGameItem[,] board;

        // Lista de elementos del juego. Si se elimina o añade un elemnto en
        // esta lista también hay que elimanarlo/añadirlo en el panel de juego (board).
        private List<IGameItem> gameItems = new List<IGameItem>();

        // Personajes mouse 
        //private Mouse myMouse = new Mouse(new ItemCoordinates(20, 1), 0);
        private SmartMouse myMouse = new SmartMouse(new ItemCoordinates(maxFila/2, 0), 0);

        // Gatos
        private List<Cat> myCats = new List<Cat>();

        //private int stepCounter = 0;

        // eventos del juego
        public event EventHandler<GameEventArgs> NewGameStepEventHandlers;
        
        /// <summary>
        /// Construye un tablero de filas x columnas
        /// </summary>
        //public GameLogic() {
        public GameLogic() {
    
            board = new IGameItem[maxFila,maxColumna];
            
            FillBoard(someFruits, somePoisons);
            
            AddItem(myMouse);

            NewGameStepEventHandlers += myMouse.UpdateGame;
            
        }

        /// <summary>
        /// Devuelve true si en la (fila, columna) especificada no hay
        /// ningún elemento de juego.
        /// </summary>
        /// <param name="fila"></param>
        /// <param name="columna"></param>
        /// <returns></returns>
        private Boolean IsCellAvailable(int fila, int columna) {
            return fila < maxFila && columna < maxColumna && fila >= 0 && fila >= 0 && board[fila, columna] == null;
        }


        /// <summary>
        /// Elimina un elemento del juego.
        /// </summary>
        /// <param name="item"></param>        
        private void RemoveItem(IGameItem item) {
            
            if (item == null) {
                return;
            }

            gameItems.Remove(item);

            if (board[item.Coords.Fila, item.Coords.Columna] == item) {
                board[item.Coords.Fila, item.Coords.Columna] = null;
            }
        }

        /// <summary>
        /// Añade un elemento al juego en la celda especificada en las coordenadas del
        /// argumento (item), siempre que (1) item != null, (2) la posición no esté ya
        /// ocupada por otro elemntoe y (3) el elemento no esté ya en el juego.
        /// </summary>
        /// <param name="item"></param>
        private void AddItem(IGameItem item) {
            
            if (item == null) {
                return;
            }

            if (!IsCellAvailable(item.Coords.Fila, item.Coords.Columna)) {
                return;
            }

            if (gameItems.Contains(item)) {
                return;
            }

            board[item.Coords.Fila, item.Coords.Columna] = item;
            gameItems.Add(item);
        }

        /// <summary>
        /// Rellena el juego con un número determinado de frutas y venenos colocados en 
        /// posiciones aleatorias.
        /// </summary>
        private void FillBoard(int nFruits, int nPoisons) {
          
            var random = new Random();
            for (var i = 0; i < nFruits; i++) {
                AddItem(new Fruit(new ItemCoordinates(random.Next(3, maxFila - 3), random.Next(3, maxColumna - 3)), 2));
            }
            for (var j = 0; j < nPoisons; j++) {
                AddItem(new Poison(new ItemCoordinates(random.Next(3, maxFila - 3), random.Next(3, maxColumna - 3)), -3));
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Fija la dirección de movimiento del ratón.
        /// </summary>
        /// <param name="next"></param>
        public void SetMouseDirection(Direction next) {
            myMouse.CurrentDirection = next;
        }

        /// <inheritdoc />
        /// <summary>
        /// Devuelve un enumerador de los elementos del juego.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<IGameItem> GetEnumerator() {
            return ((IEnumerable<IGameItem>) gameItems).GetEnumerator();
        }

        /// <summary>
        /// Implementa la lógica del juego que se ejecuta en cada Tick de un temporizador.
        /// </summary>
        /// <returns>>= 0 si el juego puede continuar, un valor negativo si no se puede continuar</returns>
        /// 
        private int timerino;
        public int ExecuteStep(GameMode mode) {
            
            // Only runs if it's not null. Nice.
            NewGameStepEventHandlers?.Invoke(this, new GameEventArgs(gameItems));

            if (mode == GameMode.AutonomousMouse) {
                myMouse.ChangeDirection();
            }
            
            // Actualiza posición del ratón de acuerdo con su dirección.
            updateMovablePosition(myMouse);

            // < 0 = mouse has died, game over
            if (processCell() < 0) {
                return -1;
            }
            
            foreach (var cat in myCats) {
                cat.ChangeDirection();
                updateMovablePosition(cat);
            }
            
            // We can never check enough for this
            if (board[myMouse.Coords.Fila,myMouse.Coords.Columna] is Cat) {
                Console.WriteLine(@"Cat hit");
                return -1;
            }
            
            // Cat spawning mechonics
            if (timerino == 20 && myCats.Count < 12) {
                var random = new Random();
                var cat = new Cat(new ItemCoordinates(0, random.Next(1, maxColumna - 1)), 0);
                myCats.Add(cat);
                AddItem(cat);
                Console.WriteLine(@"Cat spawned");
                timerino = 0;
            } else {
                timerino++;
            }

            // A cat hit the mouse, during this other phase
            if (myMouse.Health == -1) {
                Console.WriteLine(@"Cat hit");
                return -1;
            }
             
            
            // If there are no more fruits, return win status (69), else just continue (0)
            return gameItems.OfType<Fruit>().Any() ? 0 : 69;
        }

        /// <summary>
        /// Actualiza posición del elemento moviéndolo una fila o columna dependiendo
        /// de la dirección de su movimiento.
        /// Cuando el elemento llega a un límite del tablero aparece por el lado contrario.
        /// </summary>
        /// <param name="item"></param>
        private void updateMovablePosition(IMovableItem item) {

            var prevFila = item.Coords.Fila;
            var prevColumna = item.Coords.Columna;

            // Can be useful for things we are not implementing, but could totally implement. 100%.
            //var prevItem = board[prevFila, prevColumna];

            switch (item.CurrentDirection) {
                case Direction.Up:
                    item.Coords.Fila = (item.Coords.Fila - 1);
                    if (item.Coords.Fila <= 0) item.Coords.Fila = maxFila - 1;
                    break;
                case Direction.Down:
                    item.Coords.Fila = (item.Coords.Fila + 1) % maxFila;
                    break;
                case Direction.Right:
                    item.Coords.Columna = (item.Coords.Columna + 1) % maxColumna;
                    break;
                case Direction.Left:
                    item.Coords.Columna = (item.Coords.Columna - 1);
                    if (item.Coords.Columna <= 0) item.Coords.Columna = maxColumna - 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (item is Cat) {
                board[prevFila, prevColumna] = null;

                var nextSpot = board[item.Coords.Fila, item.Coords.Columna];
                if (nextSpot is Mouse) {
                    myMouse.Health = -1;
                } else {
                    RemoveItem(nextSpot);
                    board[item.Coords.Fila, item.Coords.Columna] = item;
                }
            }

        }

        /// <summary>
        /// Actualiza el juego en función de lo que hay en la celda donde está el ratón.
        /// Si no hay nada, no hace nada.
        /// Si hay fruta o veneno, suma al "valor" del ratón el valor de la fruta (positivo) o del veneno (negativo) y
        /// elimina la fruta o el veneno.
        /// Si hay un gato pone el valor del ratón en -1.
        /// </summary>
        /// <returns> El valor almacenado en el ratón. </returns>
        private int processCell() {
            
            if (board[myMouse.Coords.Fila, myMouse.Coords.Columna] is Fruit || board[myMouse.Coords.Fila, myMouse.Coords.Columna] is Poison) {
                myMouse.Health += board[myMouse.Coords.Fila, myMouse.Coords.Columna].Health;
                Console.WriteLine(myMouse + @" ate " + board[myMouse.Coords.Fila, myMouse.Coords.Columna].GetType().Name);
                Console.WriteLine(@"Current Health: " + myMouse.Health);
                
                RemoveItem(board[myMouse.Coords.Fila, myMouse.Coords.Columna]);
                
                // Game goes slightly faster when picking up stuff
                if (CatsAndMiceForm.MyTimer.Interval > 32) {
                    CatsAndMiceForm.MyTimer.Interval = CatsAndMiceForm.MyTimer.Interval - 8;
                    //Console.WriteLine(CatsAndMiceForm.MyTimer.Interval);
                }
            }

            if (!(board[myMouse.Coords.Fila, myMouse.Coords.Columna] is Cat)) return myMouse.Health;
            
            // Only if we hit a cat
            Console.WriteLine(@"Cat hit, game over");
            myMouse.Health = -1;
            return myMouse.Health;
            
        }

    }
    
}