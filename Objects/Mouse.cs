﻿using System;
using System.Dynamic;
using System.Runtime.InteropServices;

namespace CatsAndMice {
    public class Mouse : IMovableItem {
        Direction mDirection = Direction.Down;

        public Mouse() {
            Coords = new ItemCoordinates();
            Health = 0;
        }

        public Mouse(ItemCoordinates coords, int health) {
            Coords = coords;
            Health = health;
        }

        public ItemCoordinates Coords { get; set; }
        public int Health { get; set; }

        public Direction CurrentDirection {
            get { return mDirection; }
            set { mDirection = value; }
        }

        public virtual void ChangeDirection() {
        }
        
        public override string ToString() {
            return GetType().Name + " " +  Coords;
        }
    }
}