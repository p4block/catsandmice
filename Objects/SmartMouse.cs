﻿using System;
using System.Collections.Generic;

namespace CatsAndMice {
    public class SmartMouse : Mouse, IGameObserver {
        private List<IGameItem> items = new List<IGameItem>();

        public SmartMouse() {
            CurrentDirection = Direction.Right;
        }

        public SmartMouse(ItemCoordinates coords, int myValue) : base(coords, myValue){
        }

        private int currentDistance = 9999;
        private string targetString;
        
        public override void ChangeDirection() {
            
            // We pick the closes fruit and go for it, ignoring everything else. This works surprisingly well.
            // Until it doesn't.
            // This is entirely given by the status of the board at game start
            // Exactly the same as life.

            var targetColumna = 0;
            var targetFila = 0;
            IGameItem targetItem = null;
            
            foreach (var item in items) {
                if (item is Fruit) {
                    var distance = Math.Abs(Coords.Columna - item.Coords.Columna) 
                                    + Math.Abs(Coords.Fila - item.Coords.Fila);
                    if (distance < currentDistance) {
                        currentDistance = distance;
                        targetColumna = item.Coords.Columna;
                        targetFila = item.Coords.Fila;
                        targetItem = item;
                    }
                    
                }
            }
            
            if (targetColumna != Coords.Columna) {
                CurrentDirection = targetColumna > Coords.Columna ? Direction.Right : Direction.Left;
                if (CurrentDirection == Direction.Right && isDangerous(Coords.Columna,Coords.Fila)) {
                    CurrentDirection = Direction.Up;
                }
                
            } else if (targetFila != Coords.Fila) {
                CurrentDirection = targetFila > Coords.Fila ? Direction.Down : Direction.Up;
            } else {
                currentDistance = 999;
                items.Remove(targetItem);
            }
            
            var newString = "Set target " + "[" + targetColumna + "," + targetFila + "]";
            if (targetString != newString) {
                targetString = newString;
                Console.WriteLine(targetString);
            }
        }

        public void UpdateGame(object sender, GameEventArgs e) {
            
            foreach (var item in e) {
                if (!items.Contains(item)) {
                    items.Add(item);
                }
            }
            
        }

        private bool isDangerous(int columna, int fila) {
            foreach (var item in items) {
                if (item is Poison || item is Cat) {
                    if (item.Coords.Columna == columna && item.Coords.Fila == fila) {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
