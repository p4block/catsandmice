﻿using System;

namespace CatsAndMice {
    public class Cat : IMovableItem {
        
        private Direction mDirection = Direction.Down;
        
        public Cat() {
            Coords = new ItemCoordinates();
            Health = 0;
        }

        public Cat(ItemCoordinates coords, int myValue) {
            Coords = coords;
            Health = myValue;
        }
        
        public ItemCoordinates Coords { get; set; }
        public int Health { get; set; }
        
        public Direction CurrentDirection {
            get { return mDirection; }
            set { mDirection = value; }
        }
        
        public void ChangeDirection() {
            var values = Enum.GetValues(typeof(Direction));
            var random = new Random();
            var nextDirection = (Direction)values.GetValue(random.Next(values.Length));
            
            // ENTROPY++
            if (nextDirection == Direction.Up) {
                nextDirection = Direction.Down;
            } 
            
            mDirection = nextDirection;
        }

        public override string ToString() {
            return GetType().Name + " " +  Coords;
        }
    }
}