﻿namespace CatsAndMice {
    public class Fruit : IGameItem {
        
        public Fruit() {
            Coords = new ItemCoordinates();
            Health = 0;
        }

        public Fruit(ItemCoordinates coords, int myValue) {
            Coords = coords;
            Health = myValue;
        }
        
        public ItemCoordinates Coords { get; set; }
        public int Health { get; set; }
        
        public override string ToString() {
            return GetType().Name + " " +  Coords;
        }
    }
}