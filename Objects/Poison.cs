﻿namespace CatsAndMice {
    public class Poison : IGameItem {

        public Poison() {
            Health = -4;
        }

        public Poison(ItemCoordinates coords, int myValue) {
            Coords = coords;
            Health = myValue;
        }
        
        public ItemCoordinates Coords { get; set; }
        public int Health { get; set; }
        
        public override string ToString() {
            return GetType().Name + " " +  Coords;
        }
    }
}